function rgb(colors) {
  return "rgb(" + colors[0] + ", " + colors[1] + ", " + colors[2] + ")";
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getBaseColor(level) {
  const max = 256 - level / 3;
  return [getRandomInt(max), getRandomInt(max), getRandomInt(max)];
}

function getDifferentColor(level, color) {
  const dif = level / 3;
  return [color[0] + dif, color[1] + dif, color[2] + dif];
}

function resetColors() {
  color = getBaseColor(level);
  differentColor = getDifferentColor(level, color);
  differentID = getRandomInt(boxesCount);
  for (const box of boxes) {
    box.style.backgroundColor = rgb(color);
  }
  boxes[differentID].style.backgroundColor = rgb(differentColor);
}

function rewriteDetails() {
  heartsHTML.innerHTML = "قلب‌ها: " + hearts;
  scoreHTML.innerHTML = "امتیاز: " + score;
}

function gameFinish() {
  for (const box of boxes) {
    box.removeEventListener("click", box.func);
  }
}

function gameOver() {
  alert("Game Over!!!");
  gameFinish();
}

function win() {
  alert("Winner Winner! Chicken Dinner!");
  gameFinish();
}

function decision(choiceNum) {
  if (choiceNum == differentID) {
    level = level < 55 ? level - 3 : level - 9;
    score += 1;
    if (score == 45) {
      win();
    }
  } else {
    hearts -= 1;
    if (hearts == 0) {
      gameOver();
    }
  }
  resetColors();
  rewriteDetails();
}

const boxesCount = 6;
let level = 360;
let score = 0;
let hearts = 3;
let color;
let differentColor;
let differentID;

let heartsHTML = document.getElementById("hearts");
let scoreHTML = document.getElementById("score");
let boxes = document.getElementsByClassName("box");

let counter = 0;
for (const box of boxes) {
  box.gameID = counter++;
  box.func = () => {
    decision(box.gameID);
  };
  box.addEventListener("click", box.func);
}

resetColors();
